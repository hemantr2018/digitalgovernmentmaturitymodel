import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';


export let browserRefresh=false;

@Component({
  // selector: 'app-root',
  // templateUrl: './app.component.html',
  // styleUrls: ['./app.component.css']
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  isPageRefreshed: Subscription;

  constructor(private router: Router) {
    this.isPageRefreshed = router.events.subscribe(
      (event)=>{
        if (event instanceof NavigationStart) {
          browserRefresh = !router.navigated;
          if(browserRefresh)
          {
            localStorage.removeItem('quickId');
          }
        }
      }
    );
   }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

  ngOnDestroy():void{
    this.isPageRefreshed.unsubscribe();
  }
}
